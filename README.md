An example of interaction with SOAP web service.  
Simple app that displays weather information for selected city (US cities only).

[ksoap2-android](http://simpligility.github.io/ksoap2-android/index.html) libraty is used to work with SOAP.

Following services are used:  
1) http://wsf.cdyne.com/WeatherWS/Weather.asmx?WSDL  
for retrieving weather information by city ZIP code.  
2) http://www.webservicex.net/uszip.asmx?WSDL  
for retrieving ZIP code by by city name.