package ypolek.soapweather;

import android.content.Context;
import android.database.AbstractCursor;
import android.database.Cursor;
import android.text.TextUtils;
import android.widget.SimpleCursorAdapter;

import java.util.Collections;
import java.util.List;

import ypolek.soapweather.model.City;
import ypolek.soapweather.soap.GetInfoByCityAction;

public class SearchSuggestionsAdapter extends SimpleCursorAdapter {

    private static final int[] VIEW_IDS = { android.R.id.text1 };
    private static final String[] FIELDS = { "_id", "name" };
    private static final String[] VISIBLE_FIELDS = { "name" };

    public SearchSuggestionsAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_1, null, VISIBLE_FIELDS, VIEW_IDS, 0);
    }

    @Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        return new SearchSuggestionsCursor(constraint);
    }

    public City getItemByPosition(int position) {
        return ((SearchSuggestionsCursor) getItem(position)).getItem();
    }

    private static class SearchSuggestionsCursor extends AbstractCursor {

        private List<City> cities = Collections.emptyList();

        public SearchSuggestionsCursor(CharSequence query) {

            if (!TextUtils.isEmpty(query)) {
                List<City> result = new GetInfoByCityAction(query.toString()).call();
                if (result != null) cities = result;
            }
        }

        public City getItem() {
            return cities.get(getPosition());
        }

        @Override
        public int getCount() {
            return cities.size();
        }

        @Override
        public String[] getColumnNames() {
            return FIELDS;
        }

        @Override
        public long getLong(int column) {
            if (column == 0) return getPosition();
            throw new RuntimeException("Unknown column #" + column);
        }

        @Override
        public String getString(int column) {
            City city = cities.get(getPosition());
            if (column == 1) return city.name + ", " + city.state + ", " + city.zip;
            throw new RuntimeException("Unknown column #" + column);
        }

        @Override
        public short getShort(int column) {
            throw new UnsupportedOperationException("Unimplemented");
        }

        @Override
        public int getInt(int column) {
            throw new UnsupportedOperationException("Unimplemented");
        }

        @Override
        public float getFloat(int column) {
            throw new UnsupportedOperationException("Unimplemented");
        }

        @Override
        public double getDouble(int column) {
            throw new UnsupportedOperationException("Unimplemented");
        }

        @Override
        public boolean isNull(int column) {
            return false;
        }
    }
}
