package ypolek.soapweather;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import java.util.Map;

import ypolek.soapweather.model.Weather;
import ypolek.soapweather.soap.GetCityWeatherByZip;
import ypolek.soapweather.soap.GetWeatherInfoAction;

public class RetrieveWeatherHandlerThread extends HandlerThread {

    public static final int MSG_WEATHER = 1;
    public static final int MSG_WEATHER_DESCRIPTION_INFO = 2;

    private Handler handler;
    private Handler uiHandler;

    public RetrieveWeatherHandlerThread(Handler uiHandler) {
        super(RetrieveWeatherHandlerThread.class.getSimpleName());
        this.uiHandler = uiHandler;
    }

    @Override
    protected void onLooperPrepared() {
        super.onLooperPrepared();
        handler = new Handler(getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case MSG_WEATHER:
                        String zip = (String) msg.obj;
                        Weather weather = new GetCityWeatherByZip(zip).call();
                        uiHandler.sendMessage(uiHandler.obtainMessage(MSG_WEATHER, weather));
                        break;
                    case MSG_WEATHER_DESCRIPTION_INFO:
                        Map<Integer, String> weatherDescriptionInfo = new GetWeatherInfoAction().call();
                        uiHandler.sendMessage(uiHandler.obtainMessage(MSG_WEATHER_DESCRIPTION_INFO, weatherDescriptionInfo));
                        break;
                }
            }
        };
    }

    public void retrieveWeather(String zip) {
        handler.sendMessage(handler.obtainMessage(MSG_WEATHER, zip));
    }

    public void retrieveWeatherDescriptionInfo() {
        handler.sendEmptyMessage(MSG_WEATHER_DESCRIPTION_INFO);
    }
}
