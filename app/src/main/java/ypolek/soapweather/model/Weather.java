package ypolek.soapweather.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.ksoap2.serialization.SoapObject;

public class Weather implements Parcelable {

    private static final String CITY = "City";
    private static final String STATE = "State";
    private static final String WEATHER_ID = "WeatherID";
    private static final String DESCRIPTION = "Description";
    private static final String TEMPERATURE = "Temperature";
    private static final String RELATIVE_HUMIDITY = "RelativeHumidity";
    private static final String WIND = "Wind";
    private static final String PRESSURE = "Pressure";

    private static final float MM_IN_INCH = 25.4f;

    public final String city;
    public final String state;
    public final int weatherId;
    public final String description;
    public final int temperatureInCelsius;
    public final float relativeHumidity;
    public final String wind;
    public final int pressureInMm;

    public Weather(SoapObject soapObject) {
        city = soapObject.getPropertySafelyAsString(CITY);
        state = soapObject.getPropertySafelyAsString(STATE);
        weatherId = parseInt(soapObject.getPropertySafelyAsString(WEATHER_ID), -1);
        description = soapObject.getPropertySafelyAsString(DESCRIPTION);
        float tempInFahr = parseFloat(soapObject.getPropertySafelyAsString(TEMPERATURE), Float.MIN_VALUE);
        temperatureInCelsius = tempInFahr > Float.MIN_VALUE ? (int) (5f * (tempInFahr - 32f) / 9f) : Integer.MIN_VALUE;
        relativeHumidity = parseFloat(soapObject.getPropertySafelyAsString(RELATIVE_HUMIDITY), -1f);
        wind = soapObject.getPropertySafelyAsString(WIND);
        float pressureInInch = parseFloat(soapObject.getPropertySafelyAsString(PRESSURE).replaceAll("[^0-9.,]+", ""), -1f);
        pressureInMm = pressureInInch > 0f ? (int) (pressureInInch * MM_IN_INCH) : -1;
    }

    protected Weather(Parcel in) {
        city = in.readString();
        state = in.readString();
        weatherId = in.readInt();
        description = in.readString();
        temperatureInCelsius = in.readInt();
        relativeHumidity = in.readFloat();
        wind = in.readString();
        pressureInMm = in.readInt();
    }

    private int parseInt(String str, int defaultValue) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    private float parseFloat(String str, float defaultValue) {
        try {
            return Float.parseFloat(str);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    @Override
    public String toString() {
        return "Weather{" +
                "city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", weatherId=" + weatherId +
                ", description='" + description + '\'' +
                ", temperatureInCelsius=" + temperatureInCelsius +
                ", relativeHumidity=" + relativeHumidity +
                ", wind='" + wind + '\'' +
                ", pressure='" + pressureInMm + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(city);
        dest.writeString(state);
        dest.writeInt(weatherId);
        dest.writeString(description);
        dest.writeInt(temperatureInCelsius);
        dest.writeFloat(relativeHumidity);
        dest.writeString(wind);
        dest.writeInt(pressureInMm);
    }

    public static final Creator<Weather> CREATOR = new Creator<Weather>() {
        @Override
        public Weather createFromParcel(Parcel in) {
            return new Weather(in);
        }

        @Override
        public Weather[] newArray(int size) {
            return new Weather[size];
        }
    };
}
