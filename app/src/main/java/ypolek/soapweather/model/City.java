package ypolek.soapweather.model;

import android.support.annotation.NonNull;

import org.ksoap2.serialization.SoapObject;

public class City {

    private static final String PROPERTY_CITY = "CITY";
    private static final String PROPERTY_ZIP = "ZIP";
    private static final String PROPERTY_STATE = "STATE";

    public final String name;
    public final String zip;
    public final String state;

    public City(@NonNull SoapObject soapObject) {
        name = soapObject.getPropertySafelyAsString(PROPERTY_CITY);
        zip = soapObject.getPropertySafelyAsString(PROPERTY_ZIP);
        state = soapObject.getPropertySafelyAsString(PROPERTY_STATE);
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", zip='" + zip + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
