package ypolek.soapweather;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.Map;

public class BindingUtils {

    public static String getImage(Map weatherDescriptionInfo, int weatherId) {
        if (weatherDescriptionInfo == null) return null;
        return (String) weatherDescriptionInfo.get(weatherId);
    }

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Picasso.with(view.getContext())
                .load(imageUrl)
                .placeholder(null)
                .into(view);
    }
}
