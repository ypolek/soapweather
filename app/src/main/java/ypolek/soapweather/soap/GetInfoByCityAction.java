package ypolek.soapweather.soap;

import android.support.annotation.NonNull;
import android.util.Log;

import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.List;

import ypolek.soapweather.model.City;

public class GetInfoByCityAction extends BaseAction<List<City>> {

    private static final String TAG = GetInfoByCityAction.class.getSimpleName();

    private static final String URL = "http://www.webservicex.net/uszip.asmx";
    private static final String NAMESPACE = "http://www.webserviceX.NET";
    private static final String METHOD_NAME = "GetInfoByCity";
    private static final String PROPERTY_USCITY = "USCity";
    private static final String PROPERTY_RESULT_DATA_SET = "NewDataSet";

    private String cityName;

    public GetInfoByCityAction(String cityName) {
        super(URL, NAMESPACE, METHOD_NAME);
        this.cityName = cityName;
    }

    @Override
    protected void setupRequest(SoapObject request) {
        request.addProperty(PROPERTY_USCITY, cityName);
    }

    @Override
    protected List<City> parseResponse(@NonNull SoapObject response) {
        if (!response.hasProperty(PROPERTY_RESULT_DATA_SET)) {
            Log.e(TAG, "Response does not contain data set");
            return null;
        }

        SoapObject dataSet = (SoapObject) response.getProperty(PROPERTY_RESULT_DATA_SET);

        int count = dataSet.getPropertyCount();
        List<City> cities = new ArrayList<>(count);
        for (int i=0; i<count; i++) {
            SoapObject cityTable = (SoapObject) dataSet.getProperty(i);
            if (cityTable != null) {
                cities.add(new City(cityTable));
            }
        }

        return cities;
    }
}
