package ypolek.soapweather.soap;

import android.support.annotation.NonNull;
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public abstract class BaseAction<T> {

    private static final String TAG = BaseAction.class.getSimpleName();

    private String url;
    private String namespace;
    private String method;
    private String action;

    public BaseAction(String url, String namespace, String method) {
        this.url = url;
        this.namespace = namespace;
        this.method = method;
        action = namespace + (namespace.endsWith("/") ? "" : "/") + method;
    }

    public T call() {
        SoapObject request = new SoapObject(namespace, method);
        setupRequest(request);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE transport = new HttpTransportSE(url);
        try {
            transport.call(action, envelope);
        } catch (IOException | XmlPullParserException e) {
            Log.e(TAG, "failed to execute action", e);
            return null;
        }

        Object result = null;
        try {
            result = envelope.getResponse();
        } catch (SoapFault soapFault) {
            Log.e(TAG, "failed to obtain response", soapFault);
        }

        if (result instanceof SoapObject) {
            return parseResponse((SoapObject) result);
        }
        return null;
    }

    /**
     * Override this method to add request properties if required.
     * @param request request object
     */
    protected void setupRequest(SoapObject request) {

    }

    protected abstract T parseResponse(@NonNull SoapObject result);
}
