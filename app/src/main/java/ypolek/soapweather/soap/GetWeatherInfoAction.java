package ypolek.soapweather.soap;

import android.support.annotation.NonNull;

import org.ksoap2.serialization.SoapObject;

import java.util.HashMap;
import java.util.Map;

public class GetWeatherInfoAction extends BaseAction<Map<Integer/*weather ID*/, String/*picture URL*/>> {

    private static final String URL = "http://wsf.cdyne.com/WeatherWS/Weather.asmx";
    private static final String NAMESPACE = "http://ws.cdyne.com/WeatherWS/";
    private static final String METHOD_NAME = "GetWeatherInformation";
    private static final String PROPERTY_WEATHER_ID = "WeatherID";
    private static final String PROPERTY_PICTURE_URL = "PictureURL";

    public GetWeatherInfoAction() {
        super(URL, NAMESPACE, METHOD_NAME);
    }

    @Override
    protected Map<Integer, String> parseResponse(@NonNull SoapObject result) {
        Map<Integer, String> weatherInfoMap = new HashMap<>();
        for (int i=0; i<result.getPropertyCount(); i++) {
            SoapObject weatherDescription = (SoapObject) result.getProperty(i);
            Integer id;
            try {
                id = Integer.valueOf(weatherDescription.getPropertySafelyAsString(PROPERTY_WEATHER_ID));
            } catch (NumberFormatException e) {
                continue;
            }
            String pictureUrl = weatherDescription.getPropertySafelyAsString(PROPERTY_PICTURE_URL);
            weatherInfoMap.put(id, pictureUrl);
        }
        return weatherInfoMap;
    }
}
