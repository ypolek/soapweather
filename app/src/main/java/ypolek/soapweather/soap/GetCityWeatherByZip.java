package ypolek.soapweather.soap;

import android.support.annotation.NonNull;

import org.ksoap2.serialization.SoapObject;

import ypolek.soapweather.model.Weather;

public class GetCityWeatherByZip extends BaseAction<Weather> {

    private static final String URL = "http://wsf.cdyne.com/WeatherWS/Weather.asmx";
    private static final String NAMESPACE = "http://ws.cdyne.com/WeatherWS/";
    private static final String METHOD_NAME = "GetCityWeatherByZIP";
    private static final String PROPERTY_ZIP = "ZIP";
    private static final String PROPERTY_SUCCESS = "Success";

    private String zipCode;

    public GetCityWeatherByZip(String zipCode) {
        super(URL, NAMESPACE, METHOD_NAME);
        this.zipCode = zipCode;
    }

    @Override
    protected void setupRequest(SoapObject request) {
        request.addProperty(PROPERTY_ZIP, zipCode);
    }

    @Override
    protected Weather parseResponse(@NonNull SoapObject result) {
        boolean isSuccessful = Boolean.valueOf(result.getPropertySafelyAsString(PROPERTY_SUCCESS));
        return isSuccessful ? new Weather(result) : null;
    }
}
