package ypolek.soapweather;

import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.EditorInfo;
import android.widget.SearchView;

import java.io.Serializable;
import java.util.Map;

import ypolek.soapweather.databinding.MainActivityBinding;
import ypolek.soapweather.model.City;
import ypolek.soapweather.model.Weather;

public class MainActivity extends AppCompatActivity {

    private static final String KEY_WEATHER = "key_weather";
    private static final String KEY_CITY_SELECTED = "key_city_selected";
    private static final String KEY_WEATHER_DESCRIPTION_INFO = "key_weather_description_info";

    private MainActivityBinding binding;
    private RetrieveWeatherHandlerThread retrieveDataThread;
    private Map<Integer, String> weatherDescriptionInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity);

        final SearchView searchView = (SearchView) findViewById(R.id.search_view);
        assert searchView != null;
        if (Build.VERSION.SDK_INT > 16) { // avoid fullscreen keyboard to be able to show suggestion
            searchView.setImeOptions(searchView.getImeOptions() | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        }
        final SearchSuggestionsAdapter suggestionsAdapter = new SearchSuggestionsAdapter(this);
        searchView.setSuggestionsAdapter(suggestionsAdapter);
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                handleSelection(position);
                return true;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                handleSelection(position);
                return true;
            }

            private void handleSelection(int position) {
                final City city = suggestionsAdapter.getItemByPosition(position);
                retrieveDataThread.retrieveWeather(city.zip);
                if (weatherDescriptionInfo == null) {
                    retrieveDataThread.retrieveWeatherDescriptionInfo();
                } else {
                    binding.setWeatherDescriptionInfo(weatherDescriptionInfo);
                }
                searchView.clearFocus();
            }
        });

        retrieveDataThread = new RetrieveWeatherHandlerThread(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case RetrieveWeatherHandlerThread.MSG_WEATHER:
                        binding.setWeather((Weather) msg.obj);
                        binding.setCitySelected(true);
                        break;
                    case RetrieveWeatherHandlerThread.MSG_WEATHER_DESCRIPTION_INFO:
                        weatherDescriptionInfo = (Map<Integer, String>) msg.obj;
                        binding.setWeatherDescriptionInfo(weatherDescriptionInfo);
                        break;
                }
            }
        });
        retrieveDataThread.start();

        if (savedInstanceState != null) {
            binding.setWeather(savedInstanceState.<Weather>getParcelable(KEY_WEATHER));
            binding.setCitySelected(savedInstanceState.getBoolean(KEY_CITY_SELECTED));
            weatherDescriptionInfo = (Map<Integer, String>) savedInstanceState.getSerializable(KEY_WEATHER_DESCRIPTION_INFO);
            binding.setWeatherDescriptionInfo(weatherDescriptionInfo);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_WEATHER, binding.getWeather());
        outState.putBoolean(KEY_CITY_SELECTED, binding.getCitySelected());
        outState.putSerializable(KEY_WEATHER_DESCRIPTION_INFO, (Serializable) weatherDescriptionInfo);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        retrieveDataThread.quit();
    }
}
